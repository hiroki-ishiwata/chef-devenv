#!/usr/bin/env bash

# upgrade packages
apt-get update
apt-get upgrade -y
apt-get install -y autoconf

/opt/chef/embedded/bin/gem install berkshelf --no-rdoc --no-ri
/opt/chef/embedded/bin/gem install knife-solo --no-rdoc --no-ri

### git install
apt-get install -y wget aptitude

pushd /usr/local/src
if [ -f git-install.sh ]; then
  rm git-install.sh
fi

cp /vagrant/bootstrap/scripts/git-install.sh .
bash git-install.sh
rm git-install.sh
popd

export PATH=/usr/local/git/bin:$PATH

# provisioning using chef
cp /vagrant/bootstrap/scripts/provisioning-by-chef.sh .
bash provisioning-by-chef.sh
rm provisioning-by-chef.sh

