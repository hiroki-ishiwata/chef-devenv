#!/bin/bash

# provisioning using chef
PROV_USER=vagrant

tmpfile=$(mktemp)
trap "rm -fv $tmpfile" EXIT

cat <<_EOS_ > $tmpfile

export PATH=/opt/git/bin:$PATH

mkdir -p ~/.ssh

if [[ ! -f ~/.ssh/id_rsa ]]; then
  ssh-keygen -t rsa -q -f ~/.ssh/id_rsa -P ""
  cat ~/.ssh/id_rsa.pub > ~/.ssh/authorized_keys
  chmod 600 ~/.ssh/authorized_keys
  echo created private key: ~/.ssh/id_rsa
fi

mkdir -p ~/chef
pushd ~/chef

if [ -d chef-devenv ]; then
  rm -rf chef-devenv
fi

cp -r /vagrant/chef-devenv ./
cd chef-devenv

/opt/chef/embedded/bin/berks install
knife solo cook localhost prepare
knife solo cook localhost nodes/devenv.json

popd
_EOS_

chmod a+r $tmpfile

sudo -H -u $PROV_USER bash $tmpfile

rm -f $tmpfile

